// JSCRIPT 200 Class 2 Homework 2

//#1 Class 1 Exercise 3 (Page 26)
let myObject = {}; // preferred, same as above
myObject = {
  firstName: 'Jian',
  lastName: 'Sorge', 
  favoriteFood: 'burrito',
  mom: {
    firstName: 'Mom',
    lastName: 'Sorge', 
    favoriteFood: 'vegetables',
  },
  dad: {
    firstName: 'Dad',
    lastName: 'Sorge',  
    favoriteFood: 'smoothies',
  }
}; // initialize with values

console.log(myObject.dad.firstName, myObject.mom.favoriteFood);


//#2 Class 1 Exercise 4 (Page)
let board = [];
board = [['-','O','-'],['-','X','O'],['X','-','X']];
board[0][2] = 'X'; // Add X in top right corner. He switched it up!
console.log(board[0]);
console.log(board[1]);
console.log(board[2]);



//#3 You are given an assignmentDate as a string in the format "month/day/year", i.e. '1/21/2019'

//     Convert this string to a Date
//     The dueDate will be exactly 7 days after the assignment date.  Create the due date as a Date instance (new Date).
//     Create an HTML time tag as a string

const assignmentDate = '1/21/2019';

// returns array, YYYY, M, D
const convertStringToDate = function(dateString) {
    // create array to store MM/DD/YYYY
    //console.log(dateString);
    const monthDayYear = dateString.split('/');
    console.log(monthDayYear);
    //convert to YYYY, M, D
    const yearMonthDay = [];
    yearMonthDay.push(monthDayYear[2]); //year
    yearMonthDay.push(monthDayYear[0]-1); // month converted to UTC
    yearMonthDay.push(monthDayYear[1]); // day
    console.log(yearMonthDay);
    return new Date(yearMonthDay[0], yearMonthDay[1], yearMonthDay[2]) // return array YYYY, MM, DD
};

var dateAsObject = convertStringToDate(assignmentDate);

console.log(dateAsObject);


// adds due date 7 days later
var dueDate = function(dateObject) {
    const year = dateObject.getFullYear();
    const month = dateObject.getMonth();
    const day = dateObject.getDate();
    return new Date(year,month,day+7);
};

console.log(dueDate(dateAsObject));

const convertToHTML = function(dateObject) {
    //console.log(dateAsObject);
    //date to string
    const year = dateObject.getFullYear();
    const month = dateObject.getMonth()+1;
    const day = dateObject.getDate();
    //console.log(year, month,day);

    //add full name for month
    var options = { month: 'long'};
    var fullMonth = new Intl.DateTimeFormat('en-US', options).format(dateObject);
    // December

    const htmlString = `<time datetime="${year}-${month}-${day}">${fullMonth} ${day}, ${year}</time>`;
    console.log(htmlString);
    return htmlString;
};

const dateAsHTML = convertToHTML(dateAsObject);
//console.log(dateAsHTML);
console.log(`Due date is ${convertToHTML(dueDate(dateAsObject))}.`)